const fastify = require('fastify')({ logger: { level: 'info' } });

fastify.register(require("@fastify/cors"), {
  origin: '*',
  allowedHeaders: ['Content-Type', 'Authorization'], // Add any other headers you need to allow
  methods: ['GET', 'POST', 'PUT', 'DELETE'] // Add any other HTTP methods you need to allow
});

fastify.register(require('@fastify/multipart'));
const fs = require('fs');
const util = require('node:util')
const { pipeline } = require('node:stream')
const pump = util.promisify(pipeline)
const path = require('path');
const axios = require('axios');
const FormData = require('form-data');

fastify.get('/', async (request, reply) => {
  reply.send({ ok: true, rows: 'SMARTHI 16file API' })
})

fastify.post('/upload', async (request, reply) => {
  const folder = './uploads';
  const allowedExtensions = ['TXT', 'txt'];
  try {
    const parts = await request.files({
      limits: {
        fileSize: 100 * 1024 * 1024,  // 100mb limit
      }
    });
    for await (const part of parts) {
      const fileName = path.basename(part.filename);
      const destination = path.join(folder, fileName);
      const stream = await pump(part.file, fs.createWriteStream(destination))
      reply.send(`File '${fileName}' has been successfully uploaded.`);
    }
  } catch (err) {
    reply.code(500).send('An error occurred while processing the file upload.');
  }
});

fastify.post('/send', async (request, reply) => {
  const { authen, url } = request.body;
  const dirname_ = './';
  let data = new FormData();
  data.append('type', 'txt');

  data.append('file', fs.createReadStream(path.join(dirname_, 'uploads', 'PAT.TXT')));
  data.append('file', fs.createReadStream(path.join(dirname_, 'uploads', 'INS.TXT')));
  data.append('file', fs.createReadStream(path.join(dirname_, 'uploads', 'OPD.TXT')));
  data.append('file', fs.createReadStream(path.join(dirname_, 'uploads', 'IPD.TXT')));
  data.append('file', fs.createReadStream(path.join(dirname_, 'uploads', 'ADP.TXT')));
  data.append('file', fs.createReadStream(path.join(dirname_, 'uploads', 'AER.TXT')));
  data.append('file', fs.createReadStream(path.join(dirname_, 'uploads', 'CHA.TXT')));
  data.append('file', fs.createReadStream(path.join(dirname_, 'uploads', 'CHT.TXT')));
  data.append('file', fs.createReadStream(path.join(dirname_, 'uploads', 'DRU.TXT')));
  data.append('file', fs.createReadStream(path.join(dirname_, 'uploads', 'IDX.TXT')));
  data.append('file', fs.createReadStream(path.join(dirname_, 'uploads', 'IOP.TXT')));
  data.append('file', fs.createReadStream(path.join(dirname_, 'uploads', 'IRF.TXT')));
  data.append('file', fs.createReadStream(path.join(dirname_, 'uploads', 'LVD.TXT')));
  data.append('file', fs.createReadStream(path.join(dirname_, 'uploads', 'ODX.TXT')));
  data.append('file', fs.createReadStream(path.join(dirname_, 'uploads', 'OOP.TXT')));
  data.append('file', fs.createReadStream(path.join(dirname_, 'uploads', 'ORF.TXT')));

  let config = {
    method: 'post',
    url: url,
    headers: {
      'Authorization': 'Bearer ' + authen,
      ...data.getHeaders()
    },
    data: data,
  };

  await axios.request(config)
    .then( async (response)  => {
      console.log('success :' ,response.data);
      return await reply.send(response.data);
    })
    .catch( async (error) => {
      console.log('error :' ,error.response.data);
      return await reply.send(error.response.data);
    });
});

fastify.listen(30033, '0.0.0.0', (err, address) => {
  if (err) {
    fastify.log.error(err);
    process.exit(1);
  }
  fastify.log.info(`Server listening at ${address}`);
});
